<?php 
//********************************************************
// Program to list all Research Wordpress Group members
// Written by Eric MATHIEU (ACT)
// Last update: 03/05/2019
//********************************************************
 
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct access!' );
}

//Initiate connection to ldap
function mydap_start($username,$password,$host) {
	global $mydap;
	if(isset($mydap)) die('Error, LDAP connection already established');

	// Connect to AD
	$mydap = ldap_connect($host) or die('Error connecting to LDAP');
	
	$ldaprdn = 'rdg-home' . "\\" . $username;
	
	ldap_set_option($mydap,LDAP_OPT_PROTOCOL_VERSION,3);
	ldap_set_option($mydap, LDAP_OPT_REFERRALS, 0);
	@ldap_bind($mydap,$ldaprdn,$password) or die('Error binding to LDAP: '.ldap_error($mydap));

	return true;
}

// Close connection to ldap
function mydap_end() {
	global $mydap;
	if(!isset($mydap)) die('Error, no LDAP connection established');

	// Close existing LDAP connection
	ldap_unbind($mydap);
}

function mydap_attributes($user_dn,$keep=false) {
	global $mydap;
	if(!isset($mydap)) die('Error, no LDAP connection established');
	if(empty($user_dn)) die('Error, no LDAP user specified');

	// Disable pagination setting, not needed for individual attribute queries
	ldap_control_paged_result($mydap,1);
 
	// Query user attributes
	$results = (($keep) ? ldap_search($mydap,$user_dn,'cn=*',$keep) : ldap_search($mydap,$user_dn,'cn=*'))
	or die('Error searching LDAP: '.ldap_error($mydap));

	$attributes = ldap_get_entries($mydap,$results);

	// Return attributes list
	if(isset($attributes[0])) return $attributes[0];
	else return array();
}

// List Groups
function mydap_groups($object_dn) {
	global $mydap;
	if(!isset($mydap)) die('Error, no LDAP connection established');
	if(empty($object_dn)) die('Error, no LDAP object specified');
	$output = array();
		
	// Query list of Groups
	$results = ldap_search($mydap,$object_dn,'cn=*',array("name")) or die('Error searching LDAP: '.ldap_error($mydap));
	$output = ldap_get_entries($mydap,$results);
	
	// Return alphabetized list
	sort($output);
	return $output;
}

//List members of a group
function mydap_members($object_dn,$group) {
	global $mydap;
	if(!isset($mydap)) die('Error, no LDAP connection established');
	if(empty($object_dn)) die('Error, no LDAP object specified');
	$output = array();

	// Query Group members
	$results = ldap_search($mydap,$object_dn,'cn='.$group) or die('Error searching LDAP: '.ldap_error($mydap));
	$entries = ldap_get_entries($mydap,$results);
	
	// build array of members for this group, first item is count - skipped using $dirty
	$i = 0;
	$members = [];
	if ( isset ($entries[0]['member']) ){ //If there are some members, we read them
		foreach($entries[0]['member'] as $member) {
				//echo '<br>'.$member;
				if ( strpos($member,'Users') > 0 ){//if it is a user and not a group
					$members[] = substr($member,3,strpos($member,',')-3);
				}
		}
	}
	if ( !isset ($members) ) return false;
	else{
		//sort ($members);
		return $members;
	}
}

// ==================================================================================
// Main loop to read groups and list members
// ==================================================================================


function list_AD_groups_members(){
	echo "<h1>List Active Directory Wordpress groups and users</h1>";
	// Some html...
	?>
	<style>
		table {
		 border-collapse: collapse;
		 border: solid 1px;
		}
		table tr, table td {
		 border: solid 1px;;
		}
	</style>
	<?php
	set_time_limit(30);
	//error_reporting(E_ALL);
	//ini_set('error_reporting', E_ALL);
	//ini_set('display_errors',1);
	$OUPath = 'OU=Wordpress,OU=ACT,OU=Information Services Directorate,OU=UserAccounts,DC=rdg-home,DC=ad,DC=rdg,DC=ac,DC=uk';

	//Check that we are in SSL mode. If not, end the script
	if($_SERVER["HTTPS"] != "on") 
	{
		echo "This webpage is not using a secured connection (https). You cannot continue using this tool.";
		exit();
	}


	////////////////////////  FORM TO PROVIDE CREDENTIALS  ////////////////////////////////
	?>
	<form method='POST' action=' ' onSubmit='window.location.reload()'>
		<div id='username_div'>
			<div class='input_div'>	
				<input type='text' name='username' id='username' placeholder='Enter username' onfocus='showmessage(\"\")'>
			</div>
		</div>
		<div id='password_div'>
			<div class='input_div'>	
				<input type='password' name='password' id='password' placeholder='Enter password' onfocus='showmessage(\"\")'>
			</div>
		</div>
		<div id='option_div'>
			<div class='input_div'>	
				Email list only?<input type='checkbox' name='emailonly' id='emailonly'>
			</div>
		</div>
		<div id='submit_div'>
			<div class='input_div'>	
				<input type='submit' name='submit' id='submit' value = 'submit'>
			</div>
		</div>
	</form>

	<?php
	////////////////////////  IF USER PROVIDED CREDENTIALS  ////////////////////////////////
	if (isset($_POST['username']) && isset($_POST['password'])) {	
		$username=trim($_POST['username']);
		$password=trim($_POST['password']);
		// Establish ldap connection
		mydap_start(
			$username,// Active Directory search user
			$password, // Active Directory search user password
			'ldap://rdg-home.ad.rdg.ac.uk' // Active Directory server
			//,389 // Port (optional)
		);

		// List rwp groups
		$groups = mydap_groups($OUPath);

		//If we ticked to display email list only
		if ( isset($_POST['emailonly']) ){
			if ( $_POST['emailonly'] == 'on' ) { 
				echo "<h2>Users emails only</h2>";
				foreach($groups as $g) {
					$groupname = $g['name'][0];
					if ( $groupname <> "" ) {
						$members = mydap_members($OUPath,$groupname);
						foreach($members as $m) {
							print_r($m.'@reading.ac.uk<br>');

						}
					}
				}
			}
		}
		else { //We display a table with group names and members
			echo "<h2>Groups and Users</h2>";
			echo "<table>";
			foreach($groups as $g) {
				//var_dump($g);
				$groupname = $g['name'][0];
				if ( $groupname <> "" ) {
					echo "<tr><td>";
					echo $groupname;
					echo "</td><td>";
					$members = mydap_members($OUPath,$groupname);

					foreach($members as $m) {
						print_r($m.'@reading.ac.uk<br>');

					}
					echo "</td></tr>";
				}
			}
			echo "</table>";
		}

		// Close connection
		mydap_end();
	}
}