# Wordpress Plugin UoR Network Overview

Adds a dashboard menu item (for admin users only) which will contain some multisite overview information, such as:
- Overall google analytics ID
- List of sites ordered by last-update date.