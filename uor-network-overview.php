<?php
/**
 * Plugin Name: UoR Network Overview
 * Plugin URI: http://research.reading.ac.uk/act
 * Version: 1.0
 * Author: Eric MATHIEU (Academic Computing Team)
 * Author URI: http://research.reading.ac.uk/act
 * Description: Multisite overview tools for the RCE team. Activate on one site, not at the network level.
 * License: GPL2
 */
 
 
if ( ! defined( 'ABSPATH' ) ) {
	die( 'No direct access!' );
}

require_once dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'include' . DIRECTORY_SEPARATOR . 'list-wp-users.php';


//Display network overview tools
function add_network_menu_uor_overview_cb(){
	?>
	<style>
		.redrow {
			background-color : #f9b5b5;
			}
	</style>
	<h1>University of Reading Overall Management Tools</h1>
	<h2>Google analytics</h2>
	<p>
	<?php  
	if ( isset($GLOBALS['trackingID']) )
		echo '<a href="https://analytics.google.com/" target="_new">Multisite google tracking ID: '.$GLOBALS['trackingID'].'</a> (To change it, edit the variable in file /plugins/uor-overall-google-analytics.php)';
	else echo " uor-overall-google-analytics plugin doesn't seem to be activated... "; 
	?>
	
	</p>
	
<?php /* FOLLOWING CODE CAN BE ACTIVATED BY FOLLOWING INSTRUCTIONS HERE: https://developers.google.com/analytics/devguides/reporting/embed/v1/getting-started 
	<!-- Step 1: Create the containing elements. -->

	<section id="auth-button"></section>
	<section id="view-selector"></section>
	<section id="timeline"></section>

	<!-- Step 2: Load the library. -->

	<script>
	(function(w,d,s,g,js,fjs){
	  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(cb){this.q.push(cb)}};
	  js=d.createElement(s);fjs=d.getElementsByTagName(s)[0];
	  js.src='https://apis.google.com/js/platform.js';
	  fjs.parentNode.insertBefore(js,fjs);js.onload=function(){g.load('analytics')};
	}(window,document,'script'));
	</script>

	<script>
	gapi.analytics.ready(function() {

	  // Step 3: Authorize the user.

	  var CLIENT_ID = 'AIzaSyCt2KBzpKKCgmzCoY-8XUoiNqyMLf7n40c';

	  gapi.analytics.auth.authorize({
		container: 'auth-button',
		clientid: CLIENT_ID,
	  });

	  // Step 4: Create the view selector.

	  var viewSelector = new gapi.analytics.ViewSelector({
		container: 'view-selector'
	  });

	  // Step 5: Create the timeline chart.

	  var timeline = new gapi.analytics.googleCharts.DataChart({
		reportType: 'ga',
		query: {
		  'dimensions': 'ga:date',
		  'metrics': 'ga:sessions',
		  'start-date': '30daysAgo',
		  'end-date': 'yesterday',
		},
		chart: {
		  type: 'LINE',
		  container: 'timeline'
		}
	  });

	  // Step 6: Hook up the components to work together.

	  gapi.analytics.auth.on('success', function(response) {
		viewSelector.execute();
	  });

	  viewSelector.on('change', function(ids) {
		var newIds = {
		  query: {
			ids: ids
		  }
		}
		timeline.set(newIds).execute();
	  });
	});
	</script>
*/ ?>

	<h2>Websites activities</h2>
	<p><?php 
	$args = array(
		'site__not_in'    => [1,2],
		'number'      => 2000,
		'offset'     => 0,
		'orderby'	 => 'last_updated',
		'order'	 => 'ASC',
	); 
	$subsites = get_sites($args);
	$current_date = date("Y-m-d");
	$threshold_date = date("Y-m-d H:i:s", strtotime($current_date." -6 months")); //Date used to highlight sites in red if older than the threshold

	echo "<b>List of websites, ordered by their latest update date</b> (red if more than 6 months)<br>";
	?>
	<table id="analytics_table" class="tablesorter">
	<thead>	<tr> <th>Site ID</th><th>Site Name</th><th>Theme</th><th>Admin email</th> <th>Last update</th></tr></thead>
	<tbody>
	<?php
	foreach( $subsites as $subsite ) {
	  $subsite_id = get_object_vars($subsite)["blog_id"];
	  $subsite_name = get_blog_details($subsite_id)->blogname;
	  switch_to_blog($subsite_id); //Need to switch to the looped blog to get the admin-email field
	  $subsite_admin_email = get_option( 'admin_email' );
	  $theme_name = get_current_theme();
	  restore_current_blog(); //Switch back to current site
	  $subsite_lastupdate = substr(get_blog_details($subsite_id)->last_updated, 0, 10); //extract the date of the last update (remove the hours)
	  $highlightrow = '';//css class to highlight this row if the last update is older than 6 months. Empty by default.
	  if ( $subsite_lastupdate < $threshold_date ) $highlightrow = 'redrow'; //Add redrow class to highlight this row if older than threshold
	  echo '<tr class="'.$highlightrow.'"><td>' . $subsite_id . ' </td><td><a href="'.get_blog_details($subsite_id)->path.'"> ' . $subsite_name . '</a></td><td>'.$theme_name.'</td><td><a href="mailto: '. $subsite_admin_email.'">'. $subsite_admin_email.'</a></td><td>'.$subsite_lastupdate.'</td></tr>';
	}
	?>
	</tbody>
	</table></p>

	<?php
}


function add_network_menu_uor_overview() {
	if( !is_multisite() ) return false;
	add_menu_page( 'UoR multisite overview', 'UoR Overview', 'manage_options', 'uor_overview', 'add_network_menu_uor_overview_cb',  'dashicons-networking', 100);	
	add_submenu_page( 'uor_overview', 'Active Directory Groups/Users', 'AD Groups/Users', 'manage_options', 'ADgroups', 'list_AD_groups_members');
}
add_action('admin_menu', 'add_network_menu_uor_overview', 15); //Add menu item near the end of the left dashboard menu

?>
